name := "editdistance"
organization := "fpinscala"
version := "0.1-SNAPSHOT"
scalaVersion := "2.13.1"

scalacOptions ++= Seq(
  "-deprecation",
  "-encoding", "UTF-8",
  "-feature",
  "-unchecked",
  "-Xlint",
  "-Xfatal-warnings",
  "-Wdead-code",
  "-Wnumeric-widen",
  "-Wvalue-discard",
  "-Wunused:patvars,privates,locals,params,-imports",
  "-Ypatmat-exhaust-depth", "40",
  "-language:higherKinds",
  "-language:implicitConversions",
)

resolvers += Resolver.sonatypeRepo("releases")
addCompilerPlugin("org.typelevel" %% "kind-projector" % "0.11.0" cross CrossVersion.full)


// Disallow some language construcs
// your bonus exercises will have to compile with these options
addCompilerPlugin("org.wartremover" %% "wartremover" % "2.4.5" cross CrossVersion.full)
scalacOptions ++= Seq(
  "-P:wartremover:traverser:org.wartremover.warts.AsInstanceOf",
  "-P:wartremover:traverser:org.wartremover.warts.IsInstanceOf",
  "-P:wartremover:traverser:org.wartremover.warts.MutableDataStructures",
  "-P:wartremover:traverser:org.wartremover.warts.Null",
  "-P:wartremover:traverser:org.wartremover.warts.Return",
  "-P:wartremover:traverser:org.wartremover.warts.Throw",
  "-P:wartremover:traverser:org.wartremover.warts.Var",
  "-P:wartremover:traverser:org.wartremover.warts.While",
)
