package huffman

sealed trait HuffmanError
case object NoFrequencies extends HuffmanError
case object ValueNotFound extends HuffmanError
case object MissingBits   extends HuffmanError
