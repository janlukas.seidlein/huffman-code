package huffman

import scala.collection._
import scala.collection.immutable.ListMap


object Main {
  def getFrequency[A](as: List[A]): Map[A, Int] = {
    as.groupBy(l => l).map(t => (t._1, t._2.length))
  }

  def createTree[A](freqInfo: Map[A, Int]): Either[HuffmanError, Node[A]] = {
    try {
      val freqList = ListMap(freqInfo.toSeq.sortBy(_._2): _*)
      val l = for ((k, v) <- freqList) yield Leaf(k, v)
      val leafList = l.toList

      val result = recursive(leafList)
      Right(result.head)

    } catch {
      case _: Throwable => Left(NoFrequencies)
    }
  }

  def recursive[A](leafList: List[Node[A]]): List[Node[A]] = {
    if (leafList.length == 1) leafList else {
      val l = leafList.head
      val r = leafList.tail.head
      val merged = (Inner(l, r, l.freq + r.freq))
      recursive((merged :: leafList.drop(2)).sortBy(_.freq))
    }
  }


  def encode[A](as: List[A], tree: Node[A]): Either[HuffmanError, List[Boolean]] = {

    val boolList = for (
      listElement <- as
    ) yield containsinTree(tree, listElement)

    if (boolList.contains(false)) {
      Left(ValueNotFound)
    } else {
      val accu = List[Boolean]()

      def go(tree: Node[A], value: A, result: List[Boolean]): List[Boolean] = tree match {
        case Leaf(_, _) => result
        case Inner(l, r, _) => if (containsinTree(l, value)) go(l, value, result.appended(true)) else go(r, value, result.appended(false))
      }

      //val result = as.flatMap(t=>go(tree,t,accu))

      Right(as.flatMap(t => go(tree, t, accu)))
    }
  }

  def containsinTree[A](tree: Node[A], value: A): Boolean = tree match {
    case Leaf(a, _) => if (a == value) true else false
    case Inner(l, r, _) => containsinTree(l, value) || containsinTree(r, value)
  }

  def decode[A](bits: List[Boolean], tree: Node[A]): Either[HuffmanError, List[A]] = {
    try {

      def traverse(remaining: Node[A], bits: List[Boolean]): List[A] = remaining match {
        case Leaf(c, _) if bits.isEmpty => List(c)
        case Leaf(c, _) => c :: traverse(tree, bits)
        case Inner(left, right,_) if bits.head == true => traverse(left, bits.tail)
        case Inner(left, right,_) => traverse(right, bits.tail)
      }


      Right(traverse(tree, bits))
    } catch {
      case _: Throwable => Left(MissingBits)
    }

  }

  def main(args: Array[String]): Unit = {
    val result = for {
      tree <- createTree(getFrequency("aaaddddddcc".getBytes.toList))
      encoded <- encode("aaaddddddcc".getBytes.toList, tree)
      decoded <- decode(encoded, tree)
    } yield (encoded, new String(decoded.toArray))
    println(result)
  }
}