package huffman

sealed trait Node[+A] {
  def freq: Int
}
final case class Inner[+A](left: Node[A], right: Node[A], freq: Int) extends Node[A]
final case class Leaf[+A](value: A, freq: Int) extends Node[A]
